FROM biocontainers/biocontainers:v1.0.0_cv4

RUN conda install -y bwa=0.7.15
RUN conda install -y samtools
COPY --chown=biodocker bwax.py /opt/conda/bin/
USER biodocker
RUN mv /opt/conda/bin/bwax.py /opt/conda/bin/bwax && chmod +x /opt/conda/bin/bwax
WORKDIR /data/

