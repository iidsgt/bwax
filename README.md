## (BWAX) BeeWax

A python wrapper for bwa and samtools to support writing to bam files as a single command.

To run this simply execute:

```
docker run sinaiiidgst/bwa:latest bwax mem --help
```

## Pushing to Docker Hub

```bash
TAG="$(git log -1 --pretty=%h)"
docker build -t "sinaiiidgst/bwa:$TAG" .
docker push "sinaiiidgst/bwa:$TAG"
```
