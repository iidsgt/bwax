#!/usr/bin/env python
import argparse
import sys
from subprocess import Popen, PIPE, STDOUT


def parse_args():
    parser = argparse.ArgumentParser(prog='BWA', add_help=False)
    parser.add_argument('--help', action='help', help='Show Help')
    subparsers = parser.add_subparsers(help='sub-command help')

    parser_mem = subparsers.add_parser('mem', add_help=False)

    parser_mem.add_argument('idxbase')
    parser_mem.add_argument('in1.fq')
    parser_mem.add_argument('in2.fq', nargs='?', default=None)
    parser_mem.add_argument('--help', action='help', help='Show Help')

    required = parser_mem.add_argument_group('Required options')
    required.add_argument('--format', required=True, action="store")
    required.add_argument('--output', required=True, action="store")

    algorithm = parser_mem.add_argument_group('Algorithm options')
    algorithm.add_argument('-t', type=int, metavar='INT', help='number of threads [1]', action='store')
    algorithm.add_argument('-k', type=int, metavar='INT', help='minimum seed length [19]', action='store')
    algorithm.add_argument('-w', type=int, metavar='INT', help='band width for banded alignment [100]', action='store')
    algorithm.add_argument('-d', type=int, metavar='INT', help='off-diagonal X-dropoff [100]', action='store')
    algorithm.add_argument('-y', type=int, metavar='INT', help='seed occurrence for the 3rd round seeding [20]', action='store')
    algorithm.add_argument('-c', type=int, metavar='INT', help='skip seeds with more than INT occurrences [500]', action='store')
    algorithm.add_argument('-W', type=int, metavar='INT', help='discard a chain if seeded bases shorter than INT [0]', action='store')
    algorithm.add_argument('-m', type=int, metavar='INT', help='perform at most INT rounds of mate rescues for each read [50]', action='store')
    algorithm.add_argument('-S', help='skip mate rescue', action="store_true")
    algorithm.add_argument('-P', help='skip pairing; mate rescue performed unless -S also in use', action="store_true")

    scoring = parser_mem.add_argument_group('Scoring options')

    scoring.add_argument('-A', type=int, action='store', metavar='INT', help='score for a sequence match, which scales options -TdBOELU unless overridden [1]')
    scoring.add_argument('-B', type=int, action='store', metavar='INT', help='penalty for a mismatch [4]')
    scoring.add_argument('-O', type=str, action='store', metavar='STR', help='gap open penalties for deletions and insertions [6,6]')
    scoring.add_argument('-E', type=str, action='store', metavar='STR', help="gap extension penalty; a gap of size k cost '{-O} + {-E}*k' [1,1]")
    scoring.add_argument('-L', type=str, action='store', metavar='STR', help="penalty for 5'- and 3'-end clipping [5,5]")
    scoring.add_argument('-U', type=int, action='store', metavar='INT', help="penalty for an unpaired read pair [17]")
    scoring.add_argument('-x', type=str, action='store', metavar='STR', help="""read type. Setting -x changes multiple parameters unless overridden [null]
                        pacbio: -k17 -W40 -r10 -A1 -B1 -O1 -E1 -L0  (PacBio reads to ref)
                        ont2d: -k14 -W20 -r10 -A1 -B1 -O1 -E1 -L0  (Oxford Nanopore 2D-reads to ref)
                        intractg: -B9 -O16 -L5  (intra-species contigs to ref)""")

    io = parser_mem.add_argument_group('Input/output options')
    io.add_argument('-p', help="smart pairing (ignoring in2.fq)", action="store_true")
    io.add_argument('-R', action='store', metavar='STR', help="read group header line such as '@RG\\tID:foo\\tSM:bar' [null]")
    io.add_argument('-H', action='store', metavar='STR', help="insert STR to header if it starts with @; or insert lines in FILE [null]")
    io.add_argument('-j', action='store_true', help='treat ALT contigs as part of the primary assembly (i.e. ignore <idxbase>.alt file)')
    io.add_argument('-5', action='store_true', help='for split alignment, take the alignment with the smallest coordinate as primary')
    io.add_argument('-K', type=int, action='store', metavar='INT', help='process INT input bases in each batch regardless of nThreads (for reproducibility) []')
    io.add_argument('-q', action='store_true', help="don't modify mapQ of supplementary alignments")
    io.add_argument('-v', type=int, action='store', metavar='INT', help='verbosity level: 1=error, 2=warning, 3=message, 4+=debugging [3]')
    io.add_argument('-T', type=int, action='store', metavar='INT', help='minimum score to output [30]')
    io.add_argument('-h', type=int, action='store', metavar='INT', help="if there are <INT hits with score >80%% of the max score, output all in XA [5,200]")
    io.add_argument('-a', action="store_true", help='output all alignments for SE or unpaired PE')
    io.add_argument('-C', action="store_true", help='append FASTA/FASTQ comment to SAM output')
    io.add_argument('-V', action="store_true", help='output the reference FASTA header in the XR tag')
    io.add_argument('-Y', action="store_true", help='use soft clipping for supplementary alignments')
    io.add_argument('-M', action="store_true", help='mark shorter split hits as secondary')
    io.add_argument('-I', type=str, action="store", metavar='STR', help="""specify the mean, standard deviation (10%% of the mean if absent), max
                     (4 sigma from the mean if absent) and min of the insert size distribution.
                     FR orientation only. [inferred]""")
    

    return parser 

def bwa_sam(args, output):
    arg_list = ['bwa', 'mem']
    arg_list = arg_list + args
    output_file = open(output, 'a')
    bwa = Popen(arg_list, stdout=output_file)
    bwa.wait()

def bwa_bam(args, output, threads):
    bwa_arg_list = ['bwa', 'mem']
    bwa_arg_list = arg_list + args
    print(" ".join(bwa_arg_list))
    bwa = Popen(bwa_arg_list, stdout=PIPE)

    samtools_arg_list = ["samtools", "view", "-@", str(threads), "-1", "-o", output, "-"]
    print(" ".join(samtools_arg_list))
    samtools = Popen(samtools_arg_list, stdin=bwa.stdout)
    bwa.wait()
    samtools.wait()
    if samtools.returncode != 0 or bwa.returncode != 0:
        sys.exit(1)
    
if __name__ == "__main__":
    parser = parse_args()
    args = parser.parse_args()
    if len(sys.argv) < 2:
        print(parser.print_help())
        sys.exit(1)
    a_rr = vars(args)
    a_list = []
    r_list = ['','']
    for key in a_rr:
        if a_rr[key] and key in ['idxbase', 'in1.fq', 'in2.fq']:
            if key == 'idxbase':
                r_list[0] = a_rr[key]
            if key == 'in1.fq':
                r_list[1] = a_rr[key]
            if key == 'in2.fq':
                r_list.append(a_rr[key])
            continue
        if a_rr[key] and key in ['output', 'format']:
            continue
        if a_rr[key]:
            if isinstance(a_rr[key], bool):
                if a_rr[key]:
                    a_list.append('-'+key)
                    continue
            else:
                a_list.append('-'+key)
                a_list.append(str(a_rr[key]))
                continue
    a_list = a_list + r_list


    if args.format in ['sam', 'bam']:

        if args.format == 'sam':
            bwa_sam(a_list, args.output)
        if args.format == 'bam':
            bwa_bam(a_list, args.output, args.t)
    else:
        sys.exit(1)

# bwax.py mem --format bam --output readsout.dat -R '@RG\tID:tumor\tSM:sample\tLB:sample\tPL:ILLUMINA\tPU:0' -t 8 -v 1 -M
